# © 2007 maro (@macarony.de - Matthias Diener)
VERSION = 1.1.2
# use "/" to be IRC compatible and ":" for sic
COMMAND_OPERATOR = /

# edit PATH to customize installation
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

CC = cc

BIN = sim csim

all: sim

sim: sim.c
	@${CC} -Os -DVERSION=\"${VERSION}\" -DCOMMAND_OPERATOR=\'${COMMAND_OPERATOR}\' -o sim sim.c
	@strip $@
	@sed 's/^\(# csim-version:\).*/\1 ${VERSION}/' < csim > csim.o
	@mv csim.o csim

man2txt:
	@sed 's/VERSION/${VERSION}/g; sSCOMMAND_OPERATORS${COMMAND_OPERATOR}Sg' < sim.1 > sim.1.t
	@man ./sim.1.t > sim.1.txt
	@rm sim.1.t

clean:
	@rm -f sim sim-${VERSION}.tar.gz sim.1.txt

dist: clean
	@mkdir -p sim-${VERSION}
	@cp -R csim Makefile README LICENSE.html LICENSE.sic sim.c sim.1 sim-${VERSION}
	@tar -cf sim-${VERSION}.tar sim-${VERSION}
	@gzip sim-${VERSION}.tar
	@rm -rf sim-${VERSION}

install: all
	@echo install \"${BIN}\" to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${BIN} ${DESTDIR}${PREFIX}/bin
	@for i in ${BIN}; do \
		chmod 755 ${DESTDIR}${PREFIX}/bin/`basename $$i`; \
	done
	@echo install man to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed 's/VERSION/${VERSION}/g; sSCOMMAND_OPERATORS${COMMAND_OPERATOR}Sg' < sim.1 > ${DESTDIR}${MANPREFIX}/man1/sim.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/sim.1

uninstall:
	@echo remove \"${BIN}\" from ${DESTDIR}${PREFIX}/bin
	@for i in ${BIN}; do \
		rm -f ${DESTDIR}${PREFIX}/bin/`basename $$i`; \
	done
	@echo remove man from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/sim.1

.PHONY: all clean dist install uninstall

